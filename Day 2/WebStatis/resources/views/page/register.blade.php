@extends('layouts.master')

@section('title')
    <h1>Buat Account Baru!</h1>
@endsection

@section('sub-title')
    <h2>Sign Up Form</h2>
@endsection

@section('content')
    <form action="/welcome" method="post">
        @csrf
        <label for="">First name:</label><br>
        <input type="text" name="firstname" id=""><br><br>
        <label for="">Last name:</label><br>
        <input type="text" name="lastname" id=""><br><br>
        <label for="">Gender:</label><br>
        <input type="radio" name="gender" value="1">Male <br>
        <input type="radio" name="gender" value="2">Female <br>
        <input type="radio" name="gender" value="3">Other <br><br>
        <label for="">Nationality:</label><br>
        <select name="nationality" id="">
            <option value="1">Indonesian</option>
            <option value="2">Malaysian</option>
            <option value="3">American</option>
        </select><br><br>
        <label for="">Language Spoken:</label><br>
        <input type="checkbox" name="bahasa" value="1">Bahasa Indonesia <br>
        <input type="checkbox" name="bahasa" value="2">English <br>
        <input type="checkbox" name="bahasa" value="3">Other <br><br>
        <label for="">Bio:</label><br>
        <textarea name="area" id="" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
    
    
    
