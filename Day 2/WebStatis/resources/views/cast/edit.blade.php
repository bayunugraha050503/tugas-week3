@extends('layouts.master')

@section('title')
    Edit Cast
@endsection
@section('sub-title')
    Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="post">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama" value="{{$cast->nama}}" class="form-control" placeholder="Masukkan nama">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ "Nama harus diisi" }}</div>
    @enderror
    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" value="{{$cast->umur}}" class="form-control" placeholder="Masukkan umur">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ "Umur harus diisi" }}</div>
    @enderror
    <div class="form-group">
        <label>Biodata</label>
        <textarea name="bio" class="form-control">{{$cast->bio}}</textarea>
    </div>
     @error('bio')
        <div class="alert alert-danger">{{ "Biodata harus diisi" }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Edit</button>
    <a href="/cast" class="btn btn-info">Kembali</a>

</form>
@endsection