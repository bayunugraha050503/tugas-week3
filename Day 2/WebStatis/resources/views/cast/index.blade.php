@extends('layouts.master')

@section('title')
    Cast Data
@endsection
@section('sub-title')
    Cast
@endsection

@section('content')
    <a href="/cast/create" class="btn btn-primary btn-sm">Tambah Data</a>
    
        <table class="table">
    <thead>
        <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $orang)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$orang->nama}}</td>
                <td>{{$orang->umur}}</td>
                <td>
                    <form action="/cast/{{$orang->id}}" method="post">
                    <a href="/cast/{{$orang->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$orang->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="delete">
                    </form>
                </td>
            </tr>
        @empty
            <h1>Data tidak ada</h1>
             @endforelse
    </tbody>
    </table>
   
@endsection