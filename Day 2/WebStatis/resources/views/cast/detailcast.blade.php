@extends('layouts.master')

@section('title')
    Cast Data
@endsection
@section('sub-title')
    Detail Cast
@endsection

@section('content')
   <h1>{{$cast->nama}}</h1>
   <p>{{$cast->bio}}</p>
    <a href="/cast" class="btn btn-primary btn-sm">Kembali</a>
@endsection