<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('page.register');
    }

    public function welcome(Request $request) {
        $firstName = $request['firstname'];
        $lastName = $request['lastname'];
        return view('page.welcome', ['firstname'=>$firstName,'lastname'=>$lastName]);
    }

    public function table() {
        return view('page.table');
    }
    public function dataTables() {
        return view('page.data-tables');
    }
    public function master() {
        return view('layouts.master');
    }
}
