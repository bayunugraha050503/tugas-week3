<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create() {
        return view('cast.create');
    }
    public function store(Request $request) {
        // validasi
        $validated = $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required',
        ]);

        // insert
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
            
        ]);
        return redirect('/cast');
    }
    public function index() {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', ['cast' => $cast]);
    }
    public function show($cast_id) {
        $cast = DB::table('cast')->find($cast_id);
        return view('cast.detailcast',['cast' => $cast]);
    }
    public function edit($cast_id) {
        $cast = DB::table('cast')->find($cast_id);
        return view('cast.edit',['cast' => $cast]);
    }
    public function update(Request $request, $cast_id) {
        // validasi
        $validated = $request->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required',
        ]);

        $affected = DB::table('cast')
              ->where('id', $cast_id)
              ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
              ]);
        return redirect('/cast');
    }
    public function destroy($cast_id) {
        $deleted = DB::table('cast')->where('id', '=', $cast_id)->delete();
        return redirect('/cast');
    }
}
